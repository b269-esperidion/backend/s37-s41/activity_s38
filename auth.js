// JSON Web token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
const jwt = require("jsonwebtoken");


/*
ANATOMY
Header - type of token and signing algorithm
Payload - contains the claims (id, email, isAdmin)
Signature - generate by putting the encoded header, the encoded payload and applying the algorithm in the header
*/

const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data,secret, {});
};