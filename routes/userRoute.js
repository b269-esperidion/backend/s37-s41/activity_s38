const express = require("express");
// Create a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));
});


//Route for user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
});


//Route for user details
router.post("/details" , (req, res) => {
	userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(
		resultFromController));
}) 


module.exports = router;